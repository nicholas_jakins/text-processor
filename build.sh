#!/bin/sh
./gradlew build
docker build --build-arg JAR_FILE=build/libs/\*.jar -t ntjakins/text-processor:latest .
docker push ntjakins/text-processor:latest