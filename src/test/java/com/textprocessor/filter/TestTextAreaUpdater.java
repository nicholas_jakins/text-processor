package com.textprocessor.filter;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaUpdater;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestTextAreaUpdater extends AbstractTestNGSpringContextTests {

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod(){
        return new Object[][] {
                {
                    new String[] {"MONEY", "YES", "SURE"},
                        "              \n" +
                        "              \n" +
                        "        S     \n" +
                        "        U     \n" +
                        "        R     \n" +
                        "     MONEY    \n" +
                        "         E    \n" +
                        "         S    \n" +
                        "              \n" +
                        "              \n",
                    new TextArea(10, 15)},
                    {new String[] {"Betty", "Botter", "bought", "some", "butter"},
                            "              \n" +
                            "              \n" +
                            "        b     \n" +
                            "       Bu     \n" +
                            "       ot     \n" +
                            "   s Betty    \n" +
                            "  boughte     \n" +
                            "   m   er     \n" +
                            "   e   r      \n" +
                            "              \n",
                            new TextArea(10, 15)
                }
        };
    }

    @Test(dataProvider = "data-provider")
    public void testTextAreaUpdater(String[] input, String expected, TextArea textArea) {
        TextAreaUpdater textAreaUpdater = new TextAreaUpdater();
        for (String word : input) {
            textAreaUpdater.updateTextArea(word, textArea);
        }
        assertEquals(textArea.toString(), expected);
    }
}