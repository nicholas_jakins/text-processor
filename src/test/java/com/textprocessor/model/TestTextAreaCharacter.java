package com.textprocessor.model;

import static com.textprocessor.TextProcessorConstants.NULL_CHAR;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestTextAreaCharacter {

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod(){
        return new Object[][] {
                {
                  generateGrid(),
                  8, 7,
                  new TextAreaCharacter('Z', 4, 4),
                  1, 2, 1, 2
                },
                {
                        generateGrid(),
                        8, 7,
                        new TextAreaCharacter('Z', 0, 0),
                        -1, -1, 6, 7
                },
                {
                        generateGrid(),
                        8, 7,
                        new TextAreaCharacter('Z', 0, 7),
                        -1, 7, 3, -1
                }
        };
    }

    @Test(dataProvider = "data-provider")
    public void testTextAreaCharacter(char[][] grid,
                                      int height,
                                      int length,
                                      TextAreaCharacter c,
                                      int expectedLeft,
                                      int expectedUp,
                                      int expectedRight,
                                      int expectedDown) {

        assertEquals(c.getFreeSpaceLeft(grid), expectedLeft);
        assertEquals(c.getFreeSpaceUp(grid), expectedUp);
        assertEquals(c.getFreeSpaceRight(grid, length), expectedRight);
        assertEquals(c.getFreeSpaceDown(grid, height), expectedDown);
    }

    private char[][] generateGrid() {
        return new char[][]
            {
                {NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, 'D',       NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, 'C',       NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, 'A',       NULL_CHAR, NULL_CHAR, NULL_CHAR, 'Z'},
                {NULL_CHAR, NULL_CHAR, 'S',       NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, 'H',       NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR},
                {NULL_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR, 'E',       NULL_CHAR, NULL_CHAR},
            };
    }
}