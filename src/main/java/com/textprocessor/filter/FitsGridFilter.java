package com.textprocessor.filter;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaCharacter;

import java.util.List;
import java.util.stream.Collectors;

public class FitsGridFilter extends AFilter {

    public FitsGridFilter(Filter filter) {
        super(filter);
    }

    @Override
    public List<TextAreaCharacter> filterCharacters(List<TextAreaCharacter> characters,
                                                    int idxOfCharacter,
                                                    String word,
                                                    TextArea textArea) {

        List<TextAreaCharacter> charactersMatching
                = super.filterCharacters(characters, idxOfCharacter, word, textArea);

        charactersMatching.forEach(c -> c.calculateProximityAttributes(idxOfCharacter, word, textArea));

        return charactersMatching.stream().filter(c -> c.isFitsHorizontally()
                || c.isFitsVertically()).collect(Collectors.toList());
    }
}
