package com.textprocessor.filter;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaCharacter;

import java.util.List;
import java.util.stream.Collectors;

public class MatchesCharacterFilter implements Filter {

    @Override
    public List<TextAreaCharacter> filterCharacters(List<TextAreaCharacter> characters,
                                                    int idxOfCharacter,
                                                    String word,
                                                    TextArea textArea) {
        return characters.stream()
                .filter(c -> c.getValue() == word.charAt(idxOfCharacter))
                .collect(Collectors.toList());
    }
}
