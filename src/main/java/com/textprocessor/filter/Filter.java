package com.textprocessor.filter;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaCharacter;

import java.util.List;

public interface Filter {
    List<TextAreaCharacter> filterCharacters(List<TextAreaCharacter> characters,
                                             int idxOfCharacter,
                                             String word,
                                             TextArea textArea);
}
