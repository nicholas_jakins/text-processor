package com.textprocessor.filter;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaCharacter;

import java.util.List;

public abstract class AFilter implements Filter {
    private Filter filter;

    public AFilter(Filter filter) {
        this.filter = filter;
    }

    @Override
    public List<TextAreaCharacter> filterCharacters(List<TextAreaCharacter> characters,
                                                    int idxOfCharacter,
                                                    String word,
                                                    TextArea textArea) {
        return filter.filterCharacters(characters, idxOfCharacter, word, textArea);
    }
}
