package com.textprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class TextProcessorApplication {
	public static void main(String[] args) {
		SpringApplication.run(TextProcessorApplication.class, args);
	}
}
