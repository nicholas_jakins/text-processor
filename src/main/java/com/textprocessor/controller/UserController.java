package com.textprocessor.controller;

import com.textprocessor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = {"http://3.72.178.131:8080", "http://localhost:8080", "http://localhost:4200"}
        , methods = {RequestMethod.POST})
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/user")
    public ResponseEntity<Object> addUser(@RequestBody String id) {
        if (!userService.findUser(id).isPresent()) {
            userService.addUser(id);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("message", "User already exists");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
