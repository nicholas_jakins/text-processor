package com.textprocessor.controller;

import com.textprocessor.configuration.GlobalConfiguration;
import com.textprocessor.model.TextAreaResponse;
import com.textprocessor.service.TextAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = {"http://3.72.178.131:8080", "http://localhost:8080", "http://localhost:4200"}
        , methods = {RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE})
public class TextAreaController {

    @Autowired
    TextAreaService service;

    @Autowired
    GlobalConfiguration globalConfiguration;

    @PutMapping("/words")
    public ResponseEntity<String> submit(@RequestBody String[] words, HttpSession session) {
        service.submit(words, session);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/words")
    public ResponseEntity<TextAreaResponse> getWords(HttpSession session) {
        return new ResponseEntity<>(new TextAreaResponse(service.getWords(session)), HttpStatus.OK);
    }

    @DeleteMapping("/words")
    public ResponseEntity<String> deleteWords(HttpSession session) {
        service.delete(session);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
