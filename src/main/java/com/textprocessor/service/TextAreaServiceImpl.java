package com.textprocessor.service;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaUpdater;
import com.textprocessor.model.user.User;
import com.textprocessor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;

import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Optional;

@Service
public class TextAreaServiceImpl implements TextAreaService {

    private static final String SPRING_SECURITY_ATTR = "SPRING_SECURITY_CONTEXT";

    @Autowired
    TextAreaUpdater textAreaUpdater;

    @Autowired
    UserRepository repository;

    private String resolvePrincipal(HttpSession session) {
        Object springSecurityContext = session.getAttribute(SPRING_SECURITY_ATTR);
        if (springSecurityContext instanceof SecurityContextImpl) {
            return ((SecurityContextImpl) springSecurityContext).getAuthentication().getPrincipal().toString();
        }
        return null;
    }

    private User getUser(String username) {
        Optional<User> userEntry = repository.findById(username);
        if (userEntry.isPresent()) {
            return userEntry.get();
        }

        throw new RuntimeException("User should exist");
    }

    @Override
    public void submit(String[] words, HttpSession session) {
         User user = getUser(resolvePrincipal(session));

         TextArea textArea = ((TextArea) SerializationUtils.deserialize(user.getTextArea()));

        if (user != null) {
            for (String word : words) {
                for (String w : word.split(" ")) {
                    textAreaUpdater.updateTextArea(w.toUpperCase(Locale.ROOT), textArea);
                }
            }
        }

        user.setTextArea(SerializationUtils.serialize(textArea));
        repository.save(user);
    }

    @Override
    public void delete(HttpSession session) {
        User user = getUser(resolvePrincipal(session));
        TextArea textArea = ((TextArea) SerializationUtils.deserialize(user.getTextArea()));
        textArea.clear();
        user.setTextArea(SerializationUtils.serialize(textArea));
        repository.save(user);
    }

    @Override
    public String getWords(HttpSession session) {
        TextArea textArea = (TextArea) SerializationUtils.deserialize(getUser(resolvePrincipal(session)).getTextArea());
        return textArea.toString();
    }
}
