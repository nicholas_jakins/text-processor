package com.textprocessor.service;

import com.textprocessor.model.user.User;

import java.util.Optional;

public interface UserService {
    void addUser(String user);
    Optional<User> findUser(String username);
}
