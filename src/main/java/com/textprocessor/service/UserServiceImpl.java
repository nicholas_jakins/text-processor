package com.textprocessor.service;

import com.textprocessor.configuration.GlobalConfiguration;
import com.textprocessor.model.TextArea;
import com.textprocessor.model.user.User;
import com.textprocessor.repository.UserRepository;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    @Autowired
    GlobalConfiguration globalConfiguration;

    @Override
    public void addUser(String username) {

        User user = new User();
        user.setUserName(username);
        user.setTextArea(SerializationUtils.serialize(new TextArea(globalConfiguration.getHeight(), globalConfiguration.getLength())));
        repository.save(user);
    }

    @Override
    public Optional<User> findUser(String username) {
        return repository.findById(username);
    }

}
