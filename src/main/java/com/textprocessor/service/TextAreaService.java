package com.textprocessor.service;

import com.textprocessor.model.TextArea;

import javax.servlet.http.HttpSession;

public interface TextAreaService {
    void submit(String[] word, HttpSession session);
    void delete(HttpSession session);
    String getWords(HttpSession session);
}
