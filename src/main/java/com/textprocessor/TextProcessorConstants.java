package com.textprocessor;

public class TextProcessorConstants {

    public static final char NULL_CHAR = '\u0000';
    public static final String TEXT_AREA_SESSION_KEY = "text-area";
}
