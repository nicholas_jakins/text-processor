package com.textprocessor.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConfiguration {

    @Value("${textarea.height}")
    private int height;

    @Value("${textarea.length}")
    private int length;

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }
}
