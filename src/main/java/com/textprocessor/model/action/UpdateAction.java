package com.textprocessor.model.action;

import com.textprocessor.model.TextArea;
import com.textprocessor.model.TextAreaCharacter;

public class UpdateAction implements Comparable<UpdateAction> {

    private double score = 0.0;
    private final String word;
    private TextAreaCharacter character;
    private final int idxOfCharacter;
    private final TextArea textArea;

    public UpdateAction(String word,
                        TextAreaCharacter character,
                        int idxOfCharacter,
                        TextArea textArea) {

        this.word = word;
        this.idxOfCharacter = idxOfCharacter;
        this.textArea = textArea;

        if (character != null) {
            this.character = new TextAreaCharacter(character.getValue(), character.getX(), character.getY());
            this.character.setFitsHorizontally(character.isFitsHorizontally());
            this.character.setFitsVertically(character.isFitsVertically());
            this.score = calculateUpdateScore();
        }

    }

    private double calculateUpdateScore() {
        return character.isFitsVertically() ?
                verticalProximityScore() :
                horizontalProximityScore();
    }

    public double getScore() {
        return score;
    }

    public String getWord() {
        return word;
    }

    public TextAreaCharacter getCharacter() {
        return character;
    }

    public int getIdxOfCharacter() {
        return idxOfCharacter;
    }

    @Override
    public int compareTo(UpdateAction o) {
        return Double.compare(o.getScore(), this.score);
    }

    public void update() {
        TextAreaCharacter c = getCharacter();

        if (c != null) {
            if (c.isFitsHorizontally()) {
                setHorizontalCharacters(c, getIdxOfCharacter(), getWord());
            } else if (c.isFitsVertically()) {
                setVerticalCharacters(c, getIdxOfCharacter(), getWord());
            }
        }
    }

    private void setHorizontalCharacters(TextAreaCharacter c,
                                         int charactersLeft,
                                         String word) {

        int x = c.getX() - charactersLeft;
        for (int i = 0; i < word.length(); i++) {
            if (x != c.getX()) {
                char newChar = word.charAt(i);
                textArea.getGrid()[c.getY()][x] = newChar;
                textArea.getCharacters().add(new TextAreaCharacter(newChar, x, c.getY()));
            }
            x++;
        }
    }

    private void setVerticalCharacters(TextAreaCharacter c,
                                       int charactersLeft,
                                       String word) {

        int y = c.getY() - charactersLeft;
        for (int i = 0; i < word.length(); i++) {
            if (y != c.getY()) {
                char newChar = word.charAt(i);
                textArea.getGrid()[y][c.getX()] = newChar;
                textArea.getCharacters().add(new TextAreaCharacter(newChar, c.getX(), y));
            }
            y++;
        }
    }

    private double horizontalProximityScore() {

        double totalProximityScore = 0.0;
        int x = this.character.getX() - idxOfCharacter;
        for (int i = 0; i < word.length(); i++) {
            if (x != this.character.getX()) {
                char newChar = word.charAt(i);
                TextAreaCharacter newCharacter = new TextAreaCharacter(newChar, x, this.character.getY());
                totalProximityScore += newCharacter.calculateVerticalProximityScore(textArea);
            }
            x++;
        }

        return totalProximityScore;
    }

    private double verticalProximityScore() {

        double totalProximityScore = 0.0;
        int y = this.character.getY() - this.idxOfCharacter;
        for (int i = 0; i < word.length(); i++) {
            if (y != this.character.getY()) {
                char newChar = word.charAt(i);
                TextAreaCharacter newCharacter = new TextAreaCharacter(newChar, this.character.getX(), y);
                totalProximityScore += newCharacter.calculateHorizontalProximityScore(textArea);
            }
            y++;
        }
        return totalProximityScore;
    }
}
