package com.textprocessor.model;

import com.textprocessor.filter.Filter;
import com.textprocessor.filter.FitsGridFilter;
import com.textprocessor.filter.MatchesCharacterFilter;
import com.textprocessor.model.action.UpdateAction;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TextAreaUpdater {

    public void updateTextArea(String word, TextArea textArea) {
        if (textArea.getCharacters().size() == 0) {
            addFirstWord(word, textArea);
        } else {
            addToExistingWord(word, textArea);
        }
    }

    private void addFirstWord(String word, TextArea textArea) {
        int xCenter = textArea.getLength() / 2;
        int yCenter = textArea.getHeight() / 2;
        int x = xCenter - word.length() / 2;

        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            textArea.getGrid()[yCenter][x] = c;
            textArea.getCharacters().add(new TextAreaCharacter(c, x, yCenter));
            x++;
        }
    }

    private void addToExistingWord(String word, TextArea textArea) {
        Filter filter = new FitsGridFilter(new MatchesCharacterFilter());
        PriorityQueue<UpdateAction> candidates = new PriorityQueue<>();

        for (int j = 0; j < word.length(); j++) {
            final int i = j;
            filter.filterCharacters(textArea.getCharacters(), i, word, textArea)
                    .stream()
                    .forEach(c -> candidates.add(new UpdateAction(word, c, i, textArea)));
        }

        if (candidates.peek() != null) {
            candidates.peek().update();
        }

        for (TextAreaCharacter c : textArea.getCharacters()) {
            c.setRelativeProximityScore(0.0);
        }
    }
}
