package com.textprocessor.model;

public class TextAreaResponse {
    private String area;

    public String getArea() {
        return area;
    }

    public TextAreaResponse(String area) {
        this.area = area;
    }
}
