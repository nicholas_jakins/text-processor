package com.textprocessor.model;

import com.textprocessor.TextProcessorConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TextArea implements Serializable {

    private int height, length;

    private char[][] grid;
    private List<TextAreaCharacter> characters = new ArrayList<>();

    public TextArea(int height, int length) {
        this.height = height;
        this.length = length;
        setNewGrid();
    }

    private void setNewGrid() {
        grid = new char[height][length];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length - 1; j++) {
                sb.append(grid[i][j] == TextProcessorConstants.NULL_CHAR ? " " : grid[i][j]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    public char[][] getGrid() {
        return grid;
    }

    public List<TextAreaCharacter> getCharacters() {
        return characters;
    }

    public void clear() {
        setNewGrid();
        characters = new ArrayList<>();
    }
}
