package com.textprocessor.model;

import com.textprocessor.TextProcessorConstants;

import java.io.Serializable;

public class TextAreaCharacter implements Comparable<TextAreaCharacter>, Serializable {

    private final char value;
    private final int x;
    private final int y;

    private boolean fitsHorizontally = false;
    private boolean fitsVertically = false;

    private double relativeProximityScore = 0.0;

    public double getRelativeProximityScore() {
        return relativeProximityScore;
    }

    public void setRelativeProximityScore(double relativeProximityScore) {
        this.relativeProximityScore = relativeProximityScore;
    }

    public boolean isFitsHorizontally() {
        return fitsHorizontally;
    }

    public boolean isFitsVertically() {
        return fitsVertically;
    }

    public void setFitsHorizontally(boolean fitsHorizontally) {
        this.fitsHorizontally = fitsHorizontally;
    }

    public void setFitsVertically(boolean fitsVertically) {
        this.fitsVertically = fitsVertically;
    }

    public TextAreaCharacter(char value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public char getValue() {
        return value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void calculateProximityAttributes(int idxOfCharacter,
                                             String word,
                                             TextArea textArea) {
        int lengthOfWord = word.length();
        int charactersRight = lengthOfWord - (idxOfCharacter + 1);
        int charactersLeft = idxOfCharacter;

        // fits horizontally
        int freeSpaceLeft = this.getFreeSpaceLeft(textArea.getGrid());
        int freeSpaceRight = this.getFreeSpaceRight(textArea.getGrid(), textArea.getLength());

        boolean fitsHorizontally = (freeSpaceLeft < 0 || freeSpaceLeft > charactersLeft)
                && (freeSpaceRight < 0 || freeSpaceRight > charactersRight);

        // fits vertically
        int freeSpaceUp = this.getFreeSpaceUp(textArea.getGrid());
        int freeSpaceDown = this.getFreeSpaceDown(textArea.getGrid(), textArea.getHeight());

        boolean fitsVertically = (freeSpaceUp < 0 || freeSpaceUp > charactersLeft)
                && (freeSpaceDown < 0 || freeSpaceDown > charactersRight);

        this.fitsVertically = fitsVertically;
        this.fitsHorizontally = fitsHorizontally;
    }

    public double calculateHorizontalProximityScore(TextArea textArea) {
        int freeSpaceLeft = this.getFreeSpaceLeft(textArea.getGrid());
        int freeSpaceRight = this.getFreeSpaceRight(textArea.getGrid(), textArea.getLength());

        boolean onlyEmptyToTheLeft = freeSpaceLeft == this.x;
        boolean onlyEmptyToTheRight = freeSpaceRight == (textArea.getLength() - (x + 1));

        double scoreLeft = freeSpaceLeft < 0 || onlyEmptyToTheLeft ? 1.0 : (double) freeSpaceLeft / x;
        double scoreRight = freeSpaceRight < 0 || onlyEmptyToTheRight ? 1.0 : (double) freeSpaceRight / (textArea.getLength() - x);
        return scoreLeft + scoreRight;
    }

    public double calculateVerticalProximityScore(TextArea textArea) {
        int freeSpaceUp = this.getFreeSpaceUp(textArea.getGrid());
        int freeSpaceDown = this.getFreeSpaceDown(textArea.getGrid(), textArea.getHeight());

        boolean onlyEmptyUp = freeSpaceUp == this.y;
        boolean onlyEmptyDown = freeSpaceDown == (textArea.getLength() - (y + 1));

        double scoreUp = freeSpaceUp < 0 || onlyEmptyUp ? 1.0 : (double) freeSpaceUp / y;
        double scoreDown = freeSpaceDown < 0 || onlyEmptyDown ? 1.0 : (double) freeSpaceDown / (textArea.getHeight() - y);
        return scoreUp + scoreDown;
    }

    public int getFreeSpaceRight(char[][] grid, int gridLength) {
        int cnt = 0;
        if (x < gridLength - 1) {
            for (int i = x + 1; i < gridLength; i++) {
                if (TextProcessorConstants.NULL_CHAR != grid[y][i]) {
                    return cnt;
                }
                cnt++;
            }
        } else {
            return -1;
        }
        return cnt;
    }

    public int getFreeSpaceLeft(char[][] grid) {
        int cnt = 0;
        if (x > 0) {
            for (int i = x - 1; i >= 0; i--) {
                if (TextProcessorConstants.NULL_CHAR != grid[y][i]) {
                    return cnt;
                }
                cnt++;
            }
        } else {
            return -1;
        }
        return cnt;
    }

    public int getFreeSpaceUp(char[][] grid) {
        int cnt = 0;
        if (y > 0) {
            for (int i = y - 1; i >= 0; i--) {
                if (TextProcessorConstants.NULL_CHAR != grid[i][x]) {
                    return cnt;
                }
                cnt++;
            }
        } else {
            return -1;
        }
        return cnt;
    }

    public int getFreeSpaceDown(char[][] grid, int textAreaHeight) {
        int cnt = 0;
        if (y < textAreaHeight - 1) {
            for (int i = y + 1; i < textAreaHeight; i++) {
                if (TextProcessorConstants.NULL_CHAR != grid[i][x]) {
                    return cnt;
                }
                cnt++;
            }
        } else {
            return -1;
        }

        return cnt;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TextAreaCharacter) {
            return this.x == ((TextAreaCharacter) o).getX()
                    && this.y == ((TextAreaCharacter) o).getY()
                    && this.value == ((TextAreaCharacter) o).getValue();
        }

        return false;
    }

    @Override
    public int compareTo(TextAreaCharacter o) {
        return Double.compare(this.relativeProximityScore, o.getRelativeProximityScore());
    }
}