package com.textprocessor.model.user;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("User")
public class User implements Serializable {

    private String id;
    private byte[] textArea;

    public byte[] getTextArea() {
        return textArea;
    }

    public void setTextArea(byte[] textArea) {
        this.textArea = textArea;
    }

    public String getUserName() {
        return id;
    }

    public void setUserName(String userName) {
        this.id = userName;
    }
}
